# Semestrální práce - Server

## Requirements

- [Docker](https://www.docker.com)
- [JDK 15](https://www.oracle.com/java/technologies/javase-downloads.html)

## Setup

1. Download & Install requirements 
2. Clone this repository
3. Start your docker application
4. Run this command: `docker volume create --name=postgres_data`
5. Run this command inside your directory with cloned repository: `docker-compose up`
6. Make sure you're running jar file with JDK 15!

## Database structure

![Database Structure](./dbDesign.PNG)

## Requests

- All requests accept JSON content type
- All response returns content in JSON format

### POST
- `/api/v1/student` - creates row in student table
    - Body Example: 
      ```json
      {
        "firstName": "Peter",
        "lastName":  "Parker",
        "gpa": 1.2       
      }
      ```
- `/api/v1/teacher` - creates row in teacher table
    - Body Example: 
      ```json
      {
        "firstName": "Peter",
        "lastName":  "Parker"     
      }
      ```
- `/api/v1/subject` - creates row in subject table
    - Body Example: 
      ```json
      {
        "name": "Java",
        "lang":  "CZ",
        "teacherId": 4,
        "students": [1,2,3]     
      }
      ```
### PUT
- `/api/v1/student/{id}` - updates row in student table with given `{id}`
    - Body Example: 
      ```json
      {
        "firstName": "Peter",
        "lastName":  "Parker",
        "gpa": 1.2       
      }
      ```
- `/api/v1/teacher/{id}` - updates row in teacher table with given `{id}`
    - Body Example: 
      ```json
      {
        "firstName": "Peter",
        "lastName":  "Parker"     
      }
      ```
- `/api/v1/subject/{id}` - updates row in subject table with given `{id}`
    - Body Example: 
      ```json
      {
        "name": "Java",
        "lang":  "CZ",
        "teacherId": 4,
        "students": [1,2,3]     
      }
      ```
      
### DELETE
- `/api/v1/student/{id}` - removes row in student table with given `{id}`
- `/api/v1/teacher/{id}` - removes row in teacher table with given `{id}`
- `/api/v1/subject/{id}` - removes row in subject table with given `{id}`
- `/api/v1/teacher/no-subject` - removes all teachers without subjects

### GET
- `/api/v1/student` - returns all rows from student table
- `/api/v1/student/{id}` - returns row with given `{id}`
- `/api/v1/student/gpa/{gpa}` - return all rows which contains given `{gpa}`
- `/api/v1/student/gpa/less?gpa={gpa}` - return all rows which are lesser given `{gpa}`
- `/api/v1/student/gpa/greater?gpa={gpa}` - return all rows which are greater given `{gpa}`
- `/api/v1/student?firstName={firstName}` - returns all rows which contains `{firstName}`
- `/api/v1/student?lastName={lastName}` - returns all rows which contains `{lastName}`
- `/api/v1/student?firstName={firstName}&lastName={lastName}` - returns all rows which contains `{firstName}` & `{lastName}`

---

- `/api/v1/teacher` - returns all rows from teacher table
- `/api/v1/teacher/{id}` - returns row with given `{id}`
- `/api/v1/teacher?firstName={firstName}` - returns all rows which contains `{firstName}`
- `/api/v1/teacher?lastName={lastName}` - returns all rows which contains `{lastName}`
- `/api/v1/teacher?firstName={firstName}&lastName={lastName}` - returns all rows which contains `{firstName}` & `{lastName}`

---

- `/api/v1/subject` - returns all rows from teacher table
- `/api/v1/subject/{id}` - returns row with given `{id}`
- `/api/v1/subject?teacherId={teacherId}` - returns all rows which contains `{teacherId}`