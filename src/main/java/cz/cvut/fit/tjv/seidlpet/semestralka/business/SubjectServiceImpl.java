package cz.cvut.fit.tjv.seidlpet.semestralka.business;

import cz.cvut.fit.tjv.seidlpet.semestralka.data.StudentRepository;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.SubjectRepository;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.TeacherRepository;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.dto.SubjectCreateDTO;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.entity.Student;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.entity.Subject;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.entity.Teacher;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@Transactional
public class SubjectServiceImpl implements SubjectService {
    private final SubjectRepository subjectRepository;
    private final TeacherRepository teacherRepository;
    private final StudentRepository studentRepository;

    public SubjectServiceImpl(SubjectRepository subjectRepository, TeacherRepository teacherRepository, StudentRepository studentRepository) {
        this.subjectRepository = subjectRepository;
        this.teacherRepository = teacherRepository;
        this.studentRepository = studentRepository;
    }

    public Subject create(SubjectCreateDTO subject) {
        Teacher teacher = teacherRepository.findById(subject.getTeacherId()).orElseThrow();
        List<Student> students = studentRepository.findAllById(subject.getStudents());
        return subjectRepository.save(new Subject(subject.getName(),subject.getLang(),teacher,students));
    }

    public Optional<Subject> findById(int id) {
        return subjectRepository.findById(id);
    }

    public List<Subject> findAll() {
        return subjectRepository.findAll();
    }

    public List<Subject> findAllByTeacherId(int teacherId) {
        return subjectRepository.findAllByTeacherId(teacherId);
    }

    @Modifying
    public Subject update(int id, SubjectCreateDTO c) {
        Optional<Subject> optionalSubject = subjectRepository.findById(id);
        if(optionalSubject.isEmpty())
            throw new NoSuchElementException();

        Optional<Teacher> optionalTeacher = teacherRepository.findById(c.getTeacherId());
        if(optionalTeacher.isEmpty())
            throw new NoSuchElementException();

        List<Student> students = studentRepository.findAllById(c.getStudents());

        Subject subject = optionalSubject.get();
        subject.setName(c.getName());
        subject.setLang(c.getLang());
        subject.setTeacher(optionalTeacher.get());
        subject.setStudents(students);
        return subjectRepository.save(subject);
    }

    public void delete(Subject c) {
        subjectRepository.delete(c);
    }

    public void deleteById(int id) {
        subjectRepository.deleteById(id);
    }
}
