package cz.cvut.fit.tjv.seidlpet.semestralka.controller;

import cz.cvut.fit.tjv.seidlpet.semestralka.business.TeacherService;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.dto.TeacherCreateDTO;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.dto.TeacherDTO;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.entity.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/teacher")
public class TeacherController {
    private final TeacherService teacherService;

    @Autowired
    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Teacher create(@RequestBody Teacher teacher) {
        try {
            return teacherService.create(teacher);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/{id}")
    public TeacherDTO findById(@PathVariable int id) {
        return toDTO(teacherService.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)));
    }

    @GetMapping
    public List<TeacherDTO> findAll(@RequestParam(required = false) String firstName, @RequestParam(required = false) String lastName) {
        if(firstName != null && lastName != null)
            return teacherService.findByFirstNameAndLastName(firstName,lastName).stream().map(this::toDTO).collect(Collectors.toList());
        if(firstName != null)
            return teacherService.findAllByFirstName(firstName).stream().map(this::toDTO).collect(Collectors.toList());
        if(lastName != null)
            return teacherService.findAllByLastName(lastName).stream().map(this::toDTO).collect(Collectors.toList());
        return teacherService.findAll().stream().map(this::toDTO).collect(Collectors.toList());
    }

    @PutMapping("/{id}")
    public void update(@PathVariable int id, @RequestBody TeacherCreateDTO teacher) {
        try {
            teacherService.update(id, teacher);
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            teacherService.deleteById(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/no-subject")
    public void deleteTeachersWithoutSubjects() {
        try {
            teacherService.deleteTeachersWithoutSubjects();
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    private TeacherDTO toDTO(Teacher teacher) {
        return new TeacherDTO(teacher.getId(),teacher.getFirstName(),teacher.getLastName());
    }
}
