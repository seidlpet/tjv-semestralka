package cz.cvut.fit.tjv.seidlpet.semestralka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.hateoas.config.EnableHypermediaSupport;

@SpringBootApplication
@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)
public class SemestralkaApplication {

    public static void main(String[] args) {
        SpringApplication.run(SemestralkaApplication.class, args);
    }

}
