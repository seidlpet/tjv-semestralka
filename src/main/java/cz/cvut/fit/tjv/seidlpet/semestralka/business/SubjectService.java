package cz.cvut.fit.tjv.seidlpet.semestralka.business;

import cz.cvut.fit.tjv.seidlpet.semestralka.data.dto.SubjectCreateDTO;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.entity.Subject;

import java.util.List;

public interface SubjectService extends CrudService<Subject,SubjectCreateDTO> {
    Subject create(SubjectCreateDTO subject);
    List<Subject> findAllByTeacherId(int teacherId);
}
