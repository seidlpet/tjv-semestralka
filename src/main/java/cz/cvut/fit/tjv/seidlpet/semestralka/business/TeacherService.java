package cz.cvut.fit.tjv.seidlpet.semestralka.business;

import cz.cvut.fit.tjv.seidlpet.semestralka.data.dto.TeacherCreateDTO;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.entity.Teacher;

import java.util.List;

public interface TeacherService extends CrudService<Teacher, TeacherCreateDTO>{
    Teacher create(Teacher teacher);
    List<Teacher> findByFirstNameAndLastName(String firstName, String lastName);
    List<Teacher> findAllByFirstName(String firstName);
    List<Teacher> findAllByLastName(String lastName);
    void deleteTeachersWithoutSubjects();
}
