package cz.cvut.fit.tjv.seidlpet.semestralka.business;

import cz.cvut.fit.tjv.seidlpet.semestralka.data.SubjectRepository;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.TeacherRepository;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.dto.TeacherCreateDTO;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.entity.Subject;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.entity.Teacher;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@Transactional
public class TeacherServiceImpl implements TeacherService {
    private final TeacherRepository teacherRepository;
    private final SubjectRepository subjectRepository;

    public TeacherServiceImpl(TeacherRepository teacherRepository, SubjectRepository subjectRepository) {
        this.teacherRepository = teacherRepository;
        this.subjectRepository = subjectRepository;
    }

    public Teacher create(Teacher teacher) {
        return teacherRepository.save(teacher);
    }

    public Optional<Teacher> findById(int id) {
        return teacherRepository.findById(id);
    }

    public List<Teacher> findAll() {
        return teacherRepository.findAll();
    }

    public List<Teacher> findByFirstNameAndLastName(String firstName, String lastName) {
        return teacherRepository.findByFirstNameAndLastName(firstName,lastName);
    }

    public List<Teacher> findAllByFirstName(String firstName) {
        return teacherRepository.findAllByFirstName(firstName);
    }

    public List<Teacher> findAllByLastName(String lastName) {
        return teacherRepository.findAllByLastName(lastName);
    }

    @Modifying
    public Teacher update(int id, TeacherCreateDTO c) {
        Optional<Teacher> teacher = teacherRepository.findById(id);
        if(teacher.isEmpty())
            throw new NoSuchElementException();
        teacher.get().setFirstName(c.getFirstName());
        teacher.get().setLastName(c.getLastName());
        return teacherRepository.save(teacher.get());
    }

    public void delete(Teacher c) {
        teacherRepository.delete(c);
    }

    public void deleteById(int id) {
        teacherRepository.deleteById(id);
    }

    public void deleteTeachersWithoutSubjects() {
        List<Subject> subjects = subjectRepository.findAll();
        List<Teacher> teachers = findAll();
        for (Teacher teacher : teachers) {
            boolean remove = true;
            for (Subject subject : subjects) {
                if (teacher == subject.getTeacher()) {
                    remove = false;
                    break;
                }
            }
            if (remove)
                delete(teacher);
        }
    }
}
