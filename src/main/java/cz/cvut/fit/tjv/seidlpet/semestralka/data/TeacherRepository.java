package cz.cvut.fit.tjv.seidlpet.semestralka.data;

import cz.cvut.fit.tjv.seidlpet.semestralka.data.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher,Integer> {
    List<Teacher> findByFirstNameAndLastName(String firstName, String lastName);
    List<Teacher> findAllByFirstName(String firstName);
    List<Teacher> findAllByLastName(String lastName);
}
