package cz.cvut.fit.tjv.seidlpet.semestralka.controller;


import cz.cvut.fit.tjv.seidlpet.semestralka.business.SubjectService;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.dto.SubjectCreateDTO;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.dto.SubjectDTO;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.entity.Student;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.entity.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/subject")
public class SubjectController {
    private final SubjectService subjectService;

    @Autowired
    public SubjectController(SubjectService subjectService) {
        this.subjectService = subjectService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Subject create(@RequestBody SubjectCreateDTO subject) {
        try {
            return subjectService.create(subject);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/{id}")
    public SubjectDTO findById(@PathVariable int id) {
        return toDTO(subjectService.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)));
    }

    @GetMapping
    public List<SubjectDTO> findAll(@RequestParam(required = false) Integer teacherId) {
        if(teacherId != null)
            return subjectService.findAllByTeacherId(teacherId).stream().map(this::toDTO).collect(Collectors.toList());
        return subjectService.findAll().stream().map(this::toDTO).collect(Collectors.toList());
    }


    @PutMapping("/{id}")
    public void update(@PathVariable int id, @RequestBody SubjectCreateDTO subject) {
        try {
            subjectService.update(id, subject);
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            subjectService.deleteById(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    private SubjectDTO toDTO(Subject subject) {
        return new SubjectDTO(subject.getId(),subject.getName(),subject.getLang(),subject.getTeacher().getId(),subject.getStudents().stream().map(Student::getId).collect(Collectors.toList()));
    }
}
