package cz.cvut.fit.tjv.seidlpet.semestralka.business;

import cz.cvut.fit.tjv.seidlpet.semestralka.data.StudentRepository;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.dto.StudentCreateDTO;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.entity.Student;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class StudentServiceImpl implements StudentService {
    private final StudentRepository studentRepository;

    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public Student create(Student student) {
        return studentRepository.save(student);
    }

    public List<Student> findByIds(List<Integer> ids) {
        return studentRepository.findAllById(ids);
    }

    public Optional<Student> findById(int id) {
        return studentRepository.findById(id);
    }

    public List<Student> findAll() {
        return studentRepository.findAll();
    }

    public List<Student> findByGpaGreaterThan(double gpa) {
        return studentRepository.findByGpaGreaterThan(gpa);
    }

    public List<Student> findByGpaLessThan(double gpa) {
        return studentRepository.findByGpaLessThan(gpa);
    }

    public List<Student> findByGpa(double gpa) { return studentRepository.findByGpa(gpa); }

    public List<Student> findAllByFirstName(String firstName) {
        return studentRepository.findAllByFirstName(firstName);
    }

    public List<Student> findAllByLastName(String lastName) {
        return studentRepository.findAllByLastName(lastName);
    }

    public List<Student> findAllByFirstNameAndLastName(String firstName, String lastName) {
        return studentRepository.findAllByFirstNameAndLastName(firstName,lastName);
    }

    @Modifying
    public Student update(int id, StudentCreateDTO c) {
        Optional<Student> optionalStudent = studentRepository.findById(id);
        if(optionalStudent.isEmpty())
            throw new NoSuchElementException();
        Student student = optionalStudent.get();
        student.setFirstName(c.getFirstName());
        student.setLastName(c.getLastName());
        student.setGpa(c.getGpa());
        return studentRepository.save(student);
    }

    public void delete(Student c) {
        studentRepository.delete(c);
    }

    public void deleteById(int id) {
        studentRepository.deleteById(id);
    }
}
