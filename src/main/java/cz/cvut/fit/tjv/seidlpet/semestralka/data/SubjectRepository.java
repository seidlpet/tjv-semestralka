package cz.cvut.fit.tjv.seidlpet.semestralka.data;

import cz.cvut.fit.tjv.seidlpet.semestralka.data.entity.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubjectRepository extends JpaRepository<Subject,Integer> {
    List<Subject> findAllByTeacherId(int teacherId);
}
