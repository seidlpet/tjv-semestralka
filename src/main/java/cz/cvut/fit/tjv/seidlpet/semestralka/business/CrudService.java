package cz.cvut.fit.tjv.seidlpet.semestralka.business;

import java.util.List;
import java.util.Optional;

public interface CrudService<T,K> {
    Optional<T> findById(int id);
    List<T> findAll();
    T update(int id, K c);
    void delete(T c);
    void deleteById(int id);
}
