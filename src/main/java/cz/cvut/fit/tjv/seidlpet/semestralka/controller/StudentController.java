package cz.cvut.fit.tjv.seidlpet.semestralka.controller;


import cz.cvut.fit.tjv.seidlpet.semestralka.business.StudentService;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.dto.StudentCreateDTO;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.dto.StudentDTO;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/student")
public class StudentController {
    private final StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Student create(@RequestBody Student student) {
        try {
            return studentService.create(student);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/{id}")
    public StudentDTO findById(@PathVariable int id) {
        return toDTO(studentService.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)));
    }

    @GetMapping
    public List<StudentDTO> findAll(@RequestParam(required = false) String firstName, @RequestParam(required = false) String lastName) {
        if(firstName != null && lastName != null)
            return studentService.findAllByFirstNameAndLastName(firstName,lastName).stream().map(this::toDTO).collect(Collectors.toList());
        if(firstName != null)
            return studentService.findAllByFirstName(firstName).stream().map(this::toDTO).collect(Collectors.toList());
        if(lastName != null)
            return studentService.findAllByLastName(lastName).stream().map(this::toDTO).collect(Collectors.toList());
        return studentService.findAll().stream().map(this::toDTO).collect(Collectors.toList());
    }

    @GetMapping("/gpa/{gpa}")
    public List<StudentDTO> findByGpa(@PathVariable double gpa) {
        return studentService.findByGpa(gpa).stream().map(this::toDTO).collect(Collectors.toList());
    }

    @GetMapping("/gpa/less")
    public List<StudentDTO> findByGpaLessThan(@RequestParam double gpa) {
        return studentService.findByGpaLessThan(gpa).stream().map(this::toDTO).collect(Collectors.toList());
    }

    @GetMapping("/gpa/greater")
    public List<StudentDTO> findByGpaGreaterThan(@RequestParam double gpa) {
        return studentService.findByGpaGreaterThan(gpa).stream().map(this::toDTO).collect(Collectors.toList());
    }

    @PutMapping("/{id}")
    public void update(@PathVariable int id, @RequestBody StudentCreateDTO student) {
        try {
            studentService.update(id, student);
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            studentService.deleteById(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    private StudentDTO toDTO(Student student) {
        return new StudentDTO(student.getId(),student.getFirstName(),student.getLastName(),student.getGpa());
    }
}
