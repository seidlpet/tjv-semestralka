package cz.cvut.fit.tjv.seidlpet.semestralka.data.dto;

public class StudentCreateDTO {
    private final String firstName;
    private final String lastName;
    private final double gpa;

    public StudentCreateDTO(String firstName, String lastName, double gpa) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gpa = gpa;
    }


    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public double getGpa() {
        return gpa;
    }
}