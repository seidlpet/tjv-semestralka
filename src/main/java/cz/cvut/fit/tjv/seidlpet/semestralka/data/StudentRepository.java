package cz.cvut.fit.tjv.seidlpet.semestralka.data;

import cz.cvut.fit.tjv.seidlpet.semestralka.data.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<Student,Integer> {
    List<Student> findAllByFirstName(String firstName);
    List<Student> findAllByLastName(String lastName);
    List<Student> findAllByFirstNameAndLastName(String firstName, String lastName);
    List<Student> findByGpaLessThan(double gpa);
    List<Student> findByGpaGreaterThan(double gpa);
    List<Student> findByGpa(double gpa);
}
