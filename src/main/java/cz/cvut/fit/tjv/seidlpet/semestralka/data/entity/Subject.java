package cz.cvut.fit.tjv.seidlpet.semestralka.data.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;

@Entity
public class Subject {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @NotNull
    private String name;
    @NotNull
    private String lang;

    @ManyToOne
    private Teacher teacher;

    @ManyToMany
    private List<Student> students;

    public Subject() {
    }

    public Subject(String name, String lang, Teacher teacher, List<Student> students) {
        this.name = name;
        this.lang = lang;
        this.teacher = teacher;
        this.students = students;
    }

    public Subject(Integer id, String name, String lang, Teacher teacher, List<Student> students) {
        this.id = id;
        this.name = name;
        this.lang = lang;
        this.teacher = teacher;
        this.students = students;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Subject that = (Subject) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
