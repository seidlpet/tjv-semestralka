package cz.cvut.fit.tjv.seidlpet.semestralka.business;

import cz.cvut.fit.tjv.seidlpet.semestralka.data.dto.StudentCreateDTO;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.entity.Student;

import java.util.List;

public interface StudentService extends CrudService<Student, StudentCreateDTO>{
    Student create(Student student);
    List<Student> findByIds(List<Integer> ids);
    List<Student> findByGpaGreaterThan(double gpa);
    List<Student> findByGpaLessThan(double gpa);
    List<Student> findAllByFirstName(String firstName);
    List<Student> findAllByLastName(String lastName);
    List<Student> findAllByFirstNameAndLastName(String firstName, String lastName);
    List<Student> findByGpa(double gpa);
}
