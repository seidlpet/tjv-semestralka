package cz.cvut.fit.tjv.seidlpet.semestralka.data.dto;

public class TeacherDTO {
    private final int id;
    private final String firstName;
    private final String lastName;

    public TeacherDTO(int id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
