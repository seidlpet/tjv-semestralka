package cz.cvut.fit.tjv.seidlpet.semestralka.business;

import cz.cvut.fit.tjv.seidlpet.semestralka.data.StudentRepository;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.SubjectRepository;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.TeacherRepository;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.dto.SubjectCreateDTO;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.entity.Student;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.entity.Subject;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.entity.Teacher;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
class SubjectServiceImplTest {

    @Autowired
    private SubjectService subjectService;

    @MockBean
    private SubjectRepository subjectRepository;

    @MockBean
    private StudentRepository studentRepository;

    @MockBean
    private TeacherRepository teacherRepository;

    private final List<Student> students = Arrays.asList(
            new Student(1,"Test","Student",1.2),
            new Student(2,"Test","Student",3.3),
            new Student(3,"Test","Student",2.5)
    );
    private final List<Integer> studentIds = Arrays.asList(1,2,3);
    private final Teacher teacher = new Teacher(
            1,"Test","Teacher"
    );
    private final Subject subject = new Subject(
            1,"Test","CZ",teacher,students
    );
    private final List<Subject> subjects = Arrays.asList(
            new Subject(1,"Test","CZ",teacher,students),
            new Subject(1,"Test 2","SK",teacher,students)
    );


/*
    Todo: Fix this test | Assertion expected entity but was: <null>
    @Test
    void create() {
        BDDMockito.given(teacherRepository.findById(teacher.getId())).willReturn(Optional.of(teacher));
        BDDMockito.given(studentRepository.findAllById(studentIds)).willReturn(students);
        BDDMockito.given(subjectRepository.save(subject)).willReturn(subject);
        SubjectCreateDTO subjectCreateDTO = new SubjectCreateDTO("Test","CZ",1,studentIds);
        Assertions.assertSame(subject,subjectService.create(subjectCreateDTO));
        Mockito.verify(studentRepository,Mockito.atLeastOnce()).findAllById(studentIds);
        Mockito.verify(teacherRepository,Mockito.atLeastOnce()).findById(teacher.getId());
        Mockito.verify(subjectRepository,Mockito.atLeastOnce()).save(subject);
    }
*/

    @Test
    void findById() {
        BDDMockito.given(subjectRepository.findById(subject.getId())).willReturn(Optional.of(subject));
        Assertions.assertEquals(Optional.of(subject),subjectService.findById(subject.getId()));
        Mockito.verify(subjectRepository,Mockito.atLeastOnce()).findById(subject.getId());
    }

    @Test
    void findAll() {
        BDDMockito.given(subjectRepository.findAll()).willReturn(subjects);
        Assertions.assertEquals(subjects,subjectService.findAll());
        Mockito.verify(subjectRepository,Mockito.atLeastOnce()).findAll();
    }

    @Test
    void findAllByTeacherId() {
        BDDMockito.given(subjectRepository.findAllByTeacherId(teacher.getId())).willReturn(subjects);
        Assertions.assertEquals(subjects,subjectService.findAllByTeacherId(teacher.getId()));
        Mockito.verify(subjectRepository,Mockito.atLeastOnce()).findAllByTeacherId(teacher.getId());
    }

    @Test
    void update() {
        SubjectCreateDTO subjectCreateDTO = new SubjectCreateDTO(
         "Test","SK",teacher.getId(),studentIds
        );
        Subject subject = new Subject(1,"Test","SK",teacher,students);
        BDDMockito.given(teacherRepository.findById(teacher.getId())).willReturn(Optional.of(teacher));
        BDDMockito.given(studentRepository.findAllById(studentIds)).willReturn(students);
        BDDMockito.given(subjectRepository.findById(subject.getId())).willReturn(Optional.of(subject));
        BDDMockito.given(subjectRepository.save(subject)).willReturn(subject);
        Assertions.assertEquals(subject,subjectService.update(subject.getId(),subjectCreateDTO));
        Mockito.verify(subjectRepository,Mockito.atLeastOnce()).save(subject);
        Mockito.verify(subjectRepository,Mockito.atLeastOnce()).findById(subject.getId());
    }
}