package cz.cvut.fit.tjv.seidlpet.semestralka.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fit.tjv.seidlpet.semestralka.business.StudentServiceImpl;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.dto.StudentCreateDTO;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.entity.Student;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@SpringBootTest
@AutoConfigureMockMvc
class StudentControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private StudentServiceImpl studentService;

    @Test
    void create() throws Exception {
        Student student = new Student("Ted","CZ",1.1);
        mvc.perform(MockMvcRequestBuilders.post("/api/v1/student")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(asJsonString(student)))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }
    @Test
    void findById() throws Exception {
        Student student = new Student(1,"Ted","Ned",2.5);
        BDDMockito.given(studentService.findById(student.getId())).willReturn(Optional.of(student));
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/student/" + student.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName", CoreMatchers.is(student.getFirstName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName", CoreMatchers.is(student.getLastName())));
        Mockito.verify(studentService,Mockito.atLeastOnce()).findById(student.getId());
    }

    @Test
    void findAll() throws Exception {
        List<Student> students = Arrays.asList(
                new Student(1,"Ted","Get",3.2),
                new Student(2,"Ted","Black",2.2),
                new Student(3,"Ned","Fes",2.0)
        );
        BDDMockito.given(studentService.findAll()).willReturn(students);
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/student")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.*.id").exists());
        Mockito.verify(studentService,Mockito.atLeastOnce()).findAll();

        List<Student> tedStudents = Arrays.asList(
                new Student(1,"Ted","Get",3.2),
                new Student(2,"Ted","Black",2.2)
        );
        BDDMockito.given(studentService.findAllByFirstName("Ted")).willReturn(tedStudents);
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/student?firstName=Ted")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.*.firstName", CoreMatchers.equalTo(Arrays.asList("Ted","Ted"))));
        Mockito.verify(studentService,Mockito.atLeastOnce()).findAllByFirstName("Ted");

        List<Student> fesStudents = Collections.singletonList(
                new Student(3, "Ned", "Fes", 2.0)
        );
        BDDMockito.given(studentService.findAllByLastName("Fes")).willReturn(fesStudents);
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/student?lastName=Fes")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.*.lastName", CoreMatchers.equalTo(Collections.singletonList("Fes"))));
        Mockito.verify(studentService,Mockito.atLeastOnce()).findAllByLastName("Fes");

        List<Student> peterParkerStudents = Arrays.asList(
                new Student(1,"Peter","Parker",1.2),
                new Student(2,"Peter","Parker",1.4)
        );
        BDDMockito.given(studentService.findAllByFirstNameAndLastName("Peter","Parker")).willReturn(peterParkerStudents);
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/student?firstName=Peter&lastName=Parker")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.*.id",CoreMatchers.equalTo(Arrays.asList(1,2))));
        Mockito.verify(studentService,Mockito.atLeastOnce()).findAllByFirstNameAndLastName("Peter","Parker");

    }

    @Test
    void findByGpa() throws Exception {
        List<Student> result = Collections.singletonList(
            new Student(1,"Ted","Parker",4.3)
        );
        BDDMockito.given(studentService.findByGpa(4.3)).willReturn(result);
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/student/gpa/4.3")
            .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.*.id",CoreMatchers.equalTo(Collections.singletonList(1))));
        Mockito.verify(studentService, Mockito.atLeastOnce()).findByGpa(4.3);
    }

    @Test
    void findByGpaGreaterThan() throws Exception {
        List<Student> result = Collections.singletonList(
                new Student(4, "Test", "Student", 2.6)
        );
        BDDMockito.given(studentService.findByGpaGreaterThan(1.5)).willReturn(result);
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/student/gpa/greater?gpa=1.5")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.*.id",CoreMatchers.equalTo(Collections.singletonList(4))));
        Mockito.verify(studentService,Mockito.atLeastOnce()).findByGpaGreaterThan(1.5);
    }


    @Test
    void findByGpaLessThan() throws Exception {
        List<Student> result = Collections.singletonList(
                new Student(2, "Test", "Student", 1.0)
        );
        BDDMockito.given(studentService.findByGpaLessThan(1.5)).willReturn(result);
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/student/gpa/less?gpa=1.5")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.*.id",CoreMatchers.equalTo(Collections.singletonList(2))));
        Mockito.verify(studentService,Mockito.atLeastOnce()).findByGpaLessThan(1.5);
    }

    @Test
    void update() throws Exception {
        StudentCreateDTO newStudent = new StudentCreateDTO("Ted","SK",1.3);
        mvc.perform(MockMvcRequestBuilders.put("/api/v1/student/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(newStudent)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void delete() throws Exception {
        Student student = new Student(1,"Johny","CZ",1.1);
        mvc.perform(MockMvcRequestBuilders.delete("/api/v1/student/" + student.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}