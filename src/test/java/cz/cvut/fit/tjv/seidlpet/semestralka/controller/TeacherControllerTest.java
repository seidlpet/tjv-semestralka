package cz.cvut.fit.tjv.seidlpet.semestralka.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fit.tjv.seidlpet.semestralka.business.SubjectServiceImpl;
import cz.cvut.fit.tjv.seidlpet.semestralka.business.TeacherServiceImpl;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.dto.TeacherCreateDTO;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.entity.Student;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.entity.Subject;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.entity.Teacher;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;


@SpringBootTest
@AutoConfigureMockMvc
class TeacherControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private TeacherServiceImpl teacherService;

    @MockBean
    private SubjectServiceImpl subjectService;

    @Test
    void create() throws Exception {
        Teacher teacher = new Teacher("Ted","CZ");
        mvc.perform(MockMvcRequestBuilders.post("/api/v1/teacher")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(asJsonString(teacher)))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    void findById() throws Exception {
        Teacher teacher = new Teacher(1,"Ted","Ned");
        BDDMockito.given(teacherService.findById(teacher.getId())).willReturn(Optional.of(teacher));
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/teacher/" + teacher.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName", CoreMatchers.is(teacher.getFirstName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName", CoreMatchers.is(teacher.getLastName())));
        Mockito.verify(teacherService,Mockito.atLeastOnce()).findById(teacher.getId());
    }

    @Test
    void findAll() throws Exception {
        // Testing no parameters
        List<Teacher> teachers = Arrays.asList(
                new Teacher(1,"Ted","Brown"),
                new Teacher(2,"Ted","White"),
                new Teacher(3,"Ned","Fes"),
                new Teacher(4,"John","Black"),
                new Teacher(5,"Peter","Black")
        );
        BDDMockito.given(teacherService.findAll()).willReturn(teachers);
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/teacher")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.*.id").exists());

        // Testing first & last name parameter
        List<Teacher> teacher = Collections.singletonList(
                new Teacher(3, "Ned", "Fes")
        );
        BDDMockito.given(teacherService.findByFirstNameAndLastName("Ned","Fes")).willReturn(teacher);
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/teacher?firstName=Ned&lastName=Fes")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath(
                        "$.*.firstName",CoreMatchers.equalTo(
                                Collections.singletonList("Ned"))))
                .andExpect(MockMvcResultMatchers.jsonPath(
                        "$.*.lastName",CoreMatchers.equalTo(
                                Collections.singletonList("Fes"))));

        // Testing first name prameter
        List<Teacher> tedTeachers = Arrays.asList(
            new Teacher(1,"Ted","Brown"),
            new Teacher(2,"Ted","White")
        );
        BDDMockito.given(teacherService.findAllByFirstName("Ted")).willReturn(tedTeachers);
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/teacher?firstName=Ted")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath(
                        "$.*.firstName",CoreMatchers.equalTo(Arrays.asList("Ted","Ted")))
                );

        // Testing last name parameter
        List<Teacher> blackTeachers = Arrays.asList(
                new Teacher(4,"John","Black"),
                new Teacher(5,"Peter","Black")
        );
        BDDMockito.given(teacherService.findAllByLastName("Black")).willReturn(blackTeachers);
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/teacher?lastName=Black")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath(
                        "$.*.lastName",
                        CoreMatchers.equalTo(Arrays.asList("Black","Black"))));

        Mockito.verify(teacherService,Mockito.atLeastOnce()).findAll();
    }

    @Test
    void update() throws Exception {
        Teacher newTeacher = new Teacher("Ted","SK");
        mvc.perform(MockMvcRequestBuilders.put("/api/v1/teacher/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(newTeacher)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void delete() throws Exception {
        Teacher teacher = new Teacher(1,"Johny","CZ");
        mvc.perform(MockMvcRequestBuilders.delete("/api/v1/teacher/" + teacher.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
        Mockito.verify(teacherService,Mockito.atLeastOnce()).deleteById(teacher.getId());
    }

    @Test
    void deleteTeachersWithoutSubject() throws Exception {
        List<Student> students = Arrays.asList(
                new Student(1,"Pepa","Námořník",1.0),
                new Student(2,"Luděk","Zbyšehlav", 2.0)
        );
        List<Teacher> teachers = Arrays.asList(
                new Teacher(1,"Tom","Radl"),
                new Teacher(2,"Alfons","Pepa")
        );
        List<Subject> subjects = Arrays.asList(
                new Subject(1, "Java","CZ",teachers.get(0),students),
                new Subject(1,"C++","SK",teachers.get(0),students)
        );
        BDDMockito.given(subjectService.findAll()).willReturn(subjects);
        BDDMockito.given(teacherService.findAll()).willReturn(teachers);
        mvc.perform(MockMvcRequestBuilders.delete("/api/v1/teacher/no-subject")
            .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
        Mockito.verify(teacherService,Mockito.atLeastOnce()).deleteTeachersWithoutSubjects();
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}