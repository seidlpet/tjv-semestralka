package cz.cvut.fit.tjv.seidlpet.semestralka.business;

import cz.cvut.fit.tjv.seidlpet.semestralka.data.TeacherRepository;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.dto.TeacherCreateDTO;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.entity.Teacher;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
class TeacherServiceImplTest {

    @Autowired
    private TeacherService teacherService;

    @MockBean
    private TeacherRepository teacherRepository;

    private final Teacher teacher = new Teacher(1,"Test","Teacher");
    private final List<Teacher> teachers = Arrays.asList(
            new Teacher(1,"Test","Teacher"),
            new Teacher(2,"Test","Teacher")
    );
    @Test
    void create() {
        BDDMockito.given(teacherRepository.save(teacher)).willReturn(teacher);
        Assertions.assertEquals(teacher,teacherService.create(teacher));
        Mockito.verify(teacherRepository,Mockito.atLeastOnce()).save(teacher);
    }

    @Test
    void findById() {
        BDDMockito.given(teacherRepository.findById(teacher.getId())).willReturn(Optional.of(teacher));
        Assertions.assertEquals(Optional.of(teacher),teacherService.findById(teacher.getId()));
        Mockito.verify(teacherRepository,Mockito.atLeastOnce()).findById(teacher.getId());
    }

    @Test
    void findAll() {
        BDDMockito.given(teacherRepository.findAll()).willReturn(teachers);
        Assertions.assertEquals(teachers,teacherService.findAll());
        Mockito.verify(teacherRepository,Mockito.atLeastOnce()).findAll();
    }


    @Test
    void findAllByFirstName() {
        BDDMockito.given(teacherRepository.findAllByFirstName("Test")).willReturn(teachers);
        Assertions.assertEquals(teachers,teacherService.findAllByFirstName("Test"));
        Mockito.verify(teacherRepository,Mockito.atLeastOnce()).findAllByFirstName("Test");
    }

    @Test
    void findAllByLastName() {
        BDDMockito.given(teacherRepository.findAllByFirstName("Teacher")).willReturn(teachers);
        Assertions.assertEquals(teachers,teacherService.findAllByFirstName("Teacher"));
        Mockito.verify(teacherRepository,Mockito.atLeastOnce()).findAllByFirstName("Teacher");
    }

    @Test
    void update() {
        Teacher newTeacher = new Teacher(1,"Test","Teacher");
        BDDMockito.given(teacherRepository.findById(teacher.getId())).willReturn(Optional.of(teacher));
        BDDMockito.given(teacherRepository.save(newTeacher)).willReturn(newTeacher);
        Assertions.assertEquals(newTeacher,teacherService.update(teacher.getId(),new TeacherCreateDTO(newTeacher.getFirstName(),newTeacher.getLastName())));
        Mockito.verify(teacherRepository,Mockito.atLeastOnce()).save(newTeacher);
    }
}