package cz.cvut.fit.tjv.seidlpet.semestralka.business;

import cz.cvut.fit.tjv.seidlpet.semestralka.data.StudentRepository;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.dto.StudentCreateDTO;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.entity.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;


@SpringBootTest
class StudentServiceImplTest {

    @Autowired
    private StudentService studentService;

    @MockBean
    private StudentRepository studentRepository;

    private final Student student = new Student(1,"Test","Student",1.0);
    private final List<Integer> ids = Arrays.asList(1,2);
    private final List<Student> students = Arrays.asList(
            new Student(1,"Test","Student",1.2),
            new Student(2,"Test","Student",1.0)
    );

    @Test
    void create() {
        BDDMockito.given(studentRepository.save(student)).willReturn(student);
        Assertions.assertEquals(student,studentService.create(student));
        Mockito.verify(studentRepository,Mockito.atLeastOnce()).save(student);
    }

    @Test
    void findByIds() {
        BDDMockito.given(studentRepository.findAllById(ids)).willReturn(students);
        Assertions.assertEquals(students,studentService.findByIds(ids));
        Mockito.verify(studentRepository,Mockito.atLeastOnce()).findAllById(ids);
    }

    @Test
    void findById() {
        BDDMockito.given(studentRepository.findById(student.getId())).willReturn(Optional.of(student));
        Assertions.assertEquals(Optional.of(student),studentService.findById(student.getId()));
        Mockito.verify(studentRepository,Mockito.atLeastOnce()).findById(student.getId());
    }

    @Test
    void findAll() {
        BDDMockito.given(studentRepository.findAll()).willReturn(students);
        Assertions.assertEquals(students,studentService.findAll());
        Mockito.verify(studentRepository,Mockito.atLeastOnce()).findAll();
    }

    @Test
    void findByGpaGreaterThan() {
        List<Student> greaterGpaStudents = Arrays.asList(
                new Student(3,"Test","Student",1.5),
                new Student(4,"Test","Student",2.6)
        );
        BDDMockito.given(studentRepository.findByGpaGreaterThan(1.2)).willReturn(greaterGpaStudents);
        Assertions.assertEquals(greaterGpaStudents,studentService.findByGpaGreaterThan(1.2));
        Mockito.verify(studentRepository,Mockito.atLeastOnce()).findByGpaGreaterThan(1.2);

    }

    @Test
    void findByGpa() {
        List<Student> students = Collections.singletonList(
                new Student(1, "Ted", "Parker", 4.2)
        );
        BDDMockito.given(studentRepository.findByGpa(4.2)).willReturn(students);
        Assertions.assertEquals(students,studentService.findByGpa(4.2));
        Mockito.verify(studentRepository,Mockito.atLeastOnce()).findByGpa(4.2);
    }

    @Test
    void findByGpaLessThan() {
        List<Student> lessGpaStudents = Arrays.asList(
                new Student(2,"Test","Student",1.0),
                new Student(3,"Test","Student",1.5),
                new Student(4,"Test","Student",2.6)
        );
        BDDMockito.given(studentRepository.findByGpaLessThan(3)).willReturn(lessGpaStudents);
        Assertions.assertEquals(lessGpaStudents,studentService.findByGpaLessThan(3));
        Mockito.verify(studentRepository,Mockito.atLeastOnce()).findByGpaLessThan(3);
    }

    @Test
    void findAllByFirstName() {
        BDDMockito.given(studentRepository.findAllByFirstName("Test")).willReturn(students);
        Assertions.assertEquals(students,studentService.findAllByFirstName("Test"));
        Mockito.verify(studentRepository,Mockito.atLeastOnce()).findAllByFirstName("Test");
    }

    @Test
    void findAllByLastName() {
        BDDMockito.given(studentRepository.findAllByFirstName("Student")).willReturn(students);
        Assertions.assertEquals(students,studentService.findAllByFirstName("Student"));
        Mockito.verify(studentRepository,Mockito.atLeastOnce()).findAllByFirstName("Student");
    }

    @Test
    void findAllByFirstNameAndLastName() {
        List<Student> peterParkerStudents = Arrays.asList(
                new Student(1,"Peter","Parker",1.2),
                new Student(2,"Peter","Parker",1.4)
        );
        BDDMockito.given(studentRepository.findAllByFirstNameAndLastName("Peter","Parker")).willReturn(peterParkerStudents);
        Assertions.assertEquals(peterParkerStudents,studentService.findAllByFirstNameAndLastName("Peter","Parker"));
        Mockito.verify(studentRepository,Mockito.atLeastOnce()).findAllByFirstNameAndLastName("Peter","Parker");
    }

    @Test
    void update() {
        Student newStudent = new Student(1,"Test","Student",2.2);
        BDDMockito.given(studentRepository.findById(student.getId())).willReturn(Optional.of(student));
        BDDMockito.given(studentRepository.save(newStudent)).willReturn(newStudent);
        Assertions.assertEquals(newStudent,studentService.update(student.getId(),new StudentCreateDTO(newStudent.getFirstName(),newStudent.getLastName(),newStudent.getGpa())));
        Mockito.verify(studentRepository,Mockito.atLeastOnce()).save(newStudent);
        Mockito.verify(studentRepository,Mockito.atLeastOnce()).findById(student.getId());
    }
}