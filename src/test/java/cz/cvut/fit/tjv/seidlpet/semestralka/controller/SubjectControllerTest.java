package cz.cvut.fit.tjv.seidlpet.semestralka.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fit.tjv.seidlpet.semestralka.business.SubjectServiceImpl;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.dto.SubjectCreateDTO;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.entity.Student;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.entity.Subject;
import cz.cvut.fit.tjv.seidlpet.semestralka.data.entity.Teacher;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.*;

@SpringBootTest
@AutoConfigureMockMvc
public class SubjectControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private SubjectServiceImpl subjectService;

    @Test
    void create() throws Exception {
        SubjectCreateDTO subjectCreateDTO = new SubjectCreateDTO("Java","CZ",1,new ArrayList<>());
        mvc.perform(MockMvcRequestBuilders.post("/api/v1/subject")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(asJsonString(subjectCreateDTO)))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    void findById() throws Exception {
        Teacher teacher = new Teacher(1, "Johny", "Black");
        Subject subject = new Subject(1, "Java", "CZ", teacher, new ArrayList<>());
        BDDMockito.given(subjectService.findById(subject.getId())).willReturn(Optional.of(subject));
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/subject/" + subject.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.is(subject.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lang", CoreMatchers.is(subject.getLang())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.teacherId", CoreMatchers.is(teacher.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.students").isEmpty());
        Mockito.verify(subjectService, Mockito.atLeastOnce()).findById(subject.getId());
    }

    @Test
    void findAll() throws Exception {
        Teacher teacher = new Teacher(1,"Black","John");
        List<Subject> subjects = Arrays.asList(
                new Subject(1,"Java","CZ",teacher,new ArrayList<>()),
                new Subject(2,"C++","EN",teacher, Collections.singletonList(
                        new Student(1, "Teo", "Smith", 1.2)
                ))
        );
        BDDMockito.given(subjectService.findAll()).willReturn(subjects);
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/subject")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.*.id").exists());

        List<Subject> subject = Collections.singletonList(new Subject(2, "C++", "EN", teacher, Collections.singletonList(
                new Student(1, "Teo", "Smith", 1.2)
        )));
        BDDMockito.given(subjectService.findAllByTeacherId(2)).willReturn(subject);
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/subject?teacherId=2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.*.id",CoreMatchers.equalTo(Collections.singletonList(2))));

        Mockito.verify(subjectService,Mockito.atLeastOnce()).findAll();
    }
    @Test
    void update() throws Exception {
        Teacher teacher = new Teacher(1, "Johny", "Black");
        SubjectCreateDTO subjectCreateDTO = new SubjectCreateDTO("Java","CZ", teacher.getId(), Arrays.asList(1,2,3));
        mvc.perform(MockMvcRequestBuilders.put("/api/v1/subject/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(subjectCreateDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void delete() throws Exception {
        Teacher teacher = new Teacher(1, "Johny", "Black");
        Subject subject = new Subject(1,"Ted","SK",teacher,new ArrayList<>());
        mvc.perform(MockMvcRequestBuilders.delete("/api/v1/subject/" + subject.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
